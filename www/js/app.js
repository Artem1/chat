/* App Module */

'use strict';

angular.module('app', [
    'ngRoute',
    'app.controllers',
    'app.services',
    'app.directives'
]);