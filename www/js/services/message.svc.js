(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('MessageService', ['$rootScope', MessageService]);

    function MessageService($rootScope) {
        console.log('Service: MessageService');

        var sock = null;
        var chuckt = null;

        var service = {
            connect: connect,
            sendMessage: sendMessage
        };

        return service;

        function connect(userName) {
            sock = new SockJS(window.location.origin + '/chat');
            chuckt = epixa.chucktify(sock);

            sock.onopen = function () {
                chuckt.emit('connected', userName);
                chuckt.on('response', function (message) {
                    $rootScope.$broadcast('message', message);
                });
            };
        }

        function sendMessage(message) {
            if(message.text) {
                chuckt.emit('request', message);
            }
        }
    }

})();