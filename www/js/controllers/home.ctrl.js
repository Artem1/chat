(function () {
    'use strict';

    angular
        .module('app.controllers')
        .controller('HomeCtrl', ['$scope', '$location', HomeCtrl]);

    function HomeCtrl($scope, $location) {
        console.log('Controller: HomeCtrl');

        $scope.confirmName = confirmName;

        function confirmName(){
            if($scope.userName){
                // Redirect to the chat page
                $location.path('/chat/' + $scope.userName);
            }
        }
    }

})();
