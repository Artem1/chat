(function () {
    'use strict';

    angular
        .module('app.controllers')
        .controller('ChatCtrl', ['$scope', '$routeParams', '$location', 'MessageService', ChatCtrl]);

    function ChatCtrl($scope, $routeParams, $location, MessageService) {
        console.log('Controller: ChatCtrl');

        $scope.userName = $routeParams.name;

        // Redirect to login page if not logged in
        if (!$scope.userName) {
            $location.path('/home');
        }

        MessageService.connect($scope.userName);

        $scope.$on('message', messageHandler);
        $scope.sendMessage = sendMessage;
        $scope.messages = [];

        function messageHandler(e, message) {
            $scope.messages.push(message);
            $scope.$apply();
        }

        function sendMessage() {
            var message = {
                text: $scope.messageText,
                name: $scope.userName
            };

            if(message.text) {
                MessageService.sendMessage(message);
                $scope.messageText = '';
            }
        }
    }

})();
