/*
 * Use this directive to set focus on specific element.
 *
 * see more here:
 * here http://stackoverflow.com/questions/14833326/how-to-set-focus-on-input-field
 */

(function () {
    'use strict';

    angular
        .module('app.directives')
        .directive('focusOn', focusOn);

    function focusOn($timeout) {
        var directive = {
            scope: { trigger: '@focusOn' },
            link: link
        };

        return directive;

        function link(scope, element, attrs) {
            console.log('Directive: focusOn');
            scope.$watch('trigger', function (value) {
                if (value === 'true') {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
        }
    }

})();