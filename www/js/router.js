(function(){
    'use strict';

    angular.module('app')
        .config(['$routeProvider', setupRouting]);

    function setupRouting ($routeProvider) {
        $routeProvider.
            when('/chat/:name', {
                templateUrl: 'views/chat.html',
                controller: 'ChatCtrl'
            }).
            when('/home', {
                templateUrl: 'views/home.html',
                controller: 'HomeCtrl'
            }).
            otherwise({
                redirectTo: '/home'
            });
    }
})();