/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var app = express();
var sockjs = require('sockjs');
var chuckt = require('chuckt');
var connections = [];

var chat = sockjs.createServer();

chat.on('connection', function(conn) {
    var chuckt = new ChuckT(conn);
    var number = connections.length;
    var userName = 'User ' + number;

    connections.push(chuckt);

    chuckt.on('request', function(message) {
        var date = new Date();
        var time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

        for (var ii=0; ii < connections.length; ii++) {
            connections[ii].emit('response', {
                text: userName + ' says: ' + message.text,
                userName: userName,
                time: time
            });
        }
    });

    conn.on('close', function() {
        var date = new Date();
        var time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

        for (var ii=0; ii < connections.length; ii++) {
            connections[ii].emit('response', {
                text: userName + ' has disconnected',
                userName: 'Server',
                time: time
            });
        }
    });

    chuckt.on('connected', function(name) {
        var date = new Date();
        var time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        userName = name;

        for(var ii = 0; ii < connections.length; ii++) {
            connections[ii].emit('response', {
                text: userName + ' has connected',
                userName: 'Server',
                time: time
            });
        }
    });
});

// All environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, '../www'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, '../www')));

// Development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);

var server = http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
chat.installHandlers(server, {prefix:'/chat'});
